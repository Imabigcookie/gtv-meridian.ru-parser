const parseProductInfo = require('./parse-product-info')

parseCategory = async (page, link) => {
  await page.goto(link)

  const productLinks = await page.$$eval('.productList > .product a.picture', links => links.map(link => link.href))

  for (const productLink of productLinks) {
    await parseProductInfo(page, productLink)
  }

  await page.goto(link)
  try {
    const categoryNextPageLink = await page.$eval('li.bx-pag-next > a', link => link.href)

    await parseCategory(page, categoryNextPageLink)
  } catch (err) {
  }
}

module.exports = parseCategory
