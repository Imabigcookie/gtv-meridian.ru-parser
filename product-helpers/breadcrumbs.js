module.exports = async (page) => {
  const allBreadcrumbs = await page.$$eval('#breadcrumbs > ul > li > a', links => links.map(link => link.title))

  allBreadcrumbs.splice(0, 2)

  return allBreadcrumbs
}
