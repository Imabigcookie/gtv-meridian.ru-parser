module.exports = async (page) => {
  try {
    const description = await page.$eval('#detailText > .changeDescription', div => div.getAttribute('data-first-value'))

    return description
  } catch (err) {
    return null
  }
}
