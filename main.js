const puppeteer = require('puppeteer');
const catalogParser = require('./parse-category');
const clearProject = require('./clear-project');

(async () => {
  await clearProject()

  const browser = await puppeteer.launch()
  const page = await browser.newPage()

  await page.goto('https://gtv-meridian.ru/')

  const catalogLinks = await page.$$eval(
    '#mainMenu > .eChild > .menuLink',
    links => links.map(link => link.href)
  )

  for (const catalogLink of catalogLinks) {
    await catalogParser(page, catalogLink)
    console.log('category parsed')
  }


  await browser.close()
  console.log('Done!')
})();
