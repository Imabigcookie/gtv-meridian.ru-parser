const XlsxPopulate = require('xlsx-populate');
const getCategories = require('./product-helpers/breadcrumbs');
const getDescription = require('./product-helpers/description');
const getCharacteristic = require('./product-helpers/characteristic');
const getRelatedProducts = require('./product-helpers/related-products');
const downloadAllProductPhotos = require('./product-helpers/product-photos');
const downloadAllProductFiles = require('./product-helpers/product-files');

module.exports = async (page, link) => {
  await page.goto(link)

  const productName = await page.$eval('#main > .limiter > h1', h1 => h1.innerText)
  const productCategories = await getCategories(page)
  const productDescription = await getDescription(page)
  const productCharacteristic = await getCharacteristic(page)
  const productCode = productCharacteristic['Код']
  const productRelatedProducts = await getRelatedProducts(page)

  if (!productCode) return

  await downloadAllProductPhotos(page, productCode)
  await downloadAllProductFiles(page, productCode)

  const xlsx = await XlsxPopulate.fromFileAsync('./products1.xlsx')
  const sheet = xlsx.sheet(0)
  const rowNumber = sheet._rows.length

  sheet.cell(`A${rowNumber}`).value(+productCode)
  sheet.cell(`B${rowNumber}`).value(productName)
  sheet.cell(`H${rowNumber}`).value(productDescription)
  sheet.cell(`I${rowNumber}`).value(productRelatedProducts)

  productCategories.forEach((category, index) => {
    sheet.cell(rowNumber, index + 3).value(category)
  })

  Object.keys(productCharacteristic).forEach(key => {
    const newKey = key.replace('(', '').replace(')', '')
    const searchStr = new RegExp(newKey)
    const foundCells = sheet.row(1).find(searchStr)
    const lastCellNumber = sheet.row(1).maxUsedColumnNumber() + 1

    if (!foundCells.length) {
      sheet.cell(1, lastCellNumber).value(newKey).style("bold", true)
      sheet.cell(rowNumber, lastCellNumber).value(productCharacteristic[key])
    } else {
      const columnNumber = foundCells[0].columnNumber()

      sheet.cell(rowNumber, columnNumber).value(productCharacteristic[key])
    }
  })

  await xlsx.toFileAsync('./products.xlsx')
}
